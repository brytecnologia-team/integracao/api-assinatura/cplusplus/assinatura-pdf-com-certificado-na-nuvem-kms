#include "service/JsonArrayService.h"

void JsonArrayService::addValor(String value)
{
    if (dados.size() > 0)
        dados += ",\n";
    dados += value;
}

std::string JsonArrayService::getString()
{
    dados = "[\n" + dados;
    dados += "\n]";

    return dados;
}