#include <stdio.h>
#include <iostream>
#include <string.h>

#include "config/certificate_config.h"
#include "config/signature_config.h"
#include "config/service_config.h"

#include "curl/GetDownloadCurl.h"
#include "curl/PostCurl.h"
#include "service/JsonService.h"

#include "util/get_url_from_json.h"

std::string * assinarKMS();
std::string getKMSData();
void addBRyKMSData(JsonService *);
void addGovBRData(JsonService *);
void addDinamoData(JsonService *);

int main(int argc, char ** argv)
{
	//Verifica se informações estão preenchidas
	if (std::string(ACCESS_TOKEN).compare("<ACCESS_TOKEN>") == 0) {
		std::cout << "É necessário configurar um token de acesso válido em ./include/config/service_config.h" << std::endl;
		return 1;
	} else if (std::string(KMS_TYPE).compare("<KMS_TYPE>") == 0) {
		std::cout << "É necessário configurar o tipo da credencial utilizada em ./include/config/certificate_config.h. Valores Disponíveis: [BRYKMS, GOVBR e DINAMO]." << std::endl;
		return 2;
	}
	
	//Assina KMS
	std::string * urls = assinarKMS();

	//Salva os documentos assinados
	for (int i = 0; i < NUMBER_OF_DOCUMENTS; i++)
	{
		GetDownloadCurl getCurl;
		getCurl.setURL(urls[i]);
		getCurl.setFile(OUTPUT_RESOURCE_FOLDER "signature-item-" + std::to_string(i + 1) + ".pdf");
		getCurl.perform();
	}

	delete[] urls;
}

std::string * assinarKMS()
{
	//Dados obrigatorios
	JsonService jsonService;
	jsonService.addObject("kms_data", getKMSData());
	jsonService.addValor("perfil", PROFILE);
	jsonService.addValor("algoritmoHash", HASH_ALGORITHM);
	std::string dados_assinatura = jsonService.getString();
	std::cout << "Post Dados Inicializar:" << dados_assinatura.c_str() << std::endl;
	
	//Dados de configuracao de imagem
	JsonService jsonImagem;
	jsonImagem.addValor("altura", "30");
	jsonImagem.addValor("largura", "100");
	jsonImagem.addValor("coordenadaY", "30");
	jsonImagem.addValor("coordenadaX", "30");
	jsonImagem.addValor("posicao", "INFERIOR_ESQUERDO");
	jsonImagem.addValor("proporcaoImagem", "30");
	std::string configuracao_imagem = jsonImagem.getString();
	std::cout << "Post configuracao Imagem:" << configuracao_imagem.c_str() << std::endl;

	//Dados de Configuracao de texto
	JsonService jsonTexto;
	jsonTexto.addValor("texto","Doc está assinado");
	jsonTexto.addValor("incluirCN","true");
	std::string configuracao_texto = jsonTexto.getString();
	std::cout << "Post configuracao Texto:" << configuracao_texto.c_str() << std::endl;

	//Realizando envio das configuracoes
	PostCurl postCurl = PostCurl();
	postCurl.setURL(URL_SERVER_PDF);
	postCurl.addHeader("Authorization", ACCESS_TOKEN);
	postCurl.addHeader("kms_type", KMS_TYPE);
	postCurl.addMime("dados_assinatura", dados_assinatura);
	postCurl.addMime("configuracao_texto", configuracao_texto);
	postCurl.addMime("configuracao_imagem", configuracao_imagem);
	postCurl.addFile("imagem", IMAGE_LOCATION);
	postCurl.addFile("imagemFundo", BACKGROUND_IMAGE_LOCATION);
	for(int i = 0; i < NUMBER_OF_DOCUMENTS; i++)
	{
		postCurl.addFile("documento", ORIGINAL_DOCUMENTS_LOCATION[i]);
	}
	postCurl.perform();

	long status_code = postCurl.getHttpCode();
	std::string * response_body = postCurl.getResponse();

	if(status_code != 200)
	{
		std::cout << "Error during signature initialization - Status code: " << status_code << std::endl;
		std::cout << response_body->c_str() << std::endl;
		exit(1);
	}

	std::cout << "JSON response: " << response_body->c_str() << std::endl;
	json_object * response_body_as_json = postCurl.getJsonResponse();

	json_object * signatures_array_json;
	json_object_object_get_ex(response_body_as_json, "documentos", &signatures_array_json);

	std::string * signatures = new std::string[NUMBER_OF_DOCUMENTS];
	for(int i = 0; i < NUMBER_OF_DOCUMENTS; i++)
	{
		//Pega assinatura
		json_object * signature_as_json = json_object_array_get_idx(signatures_array_json, i);

		//Signatures[i] = url para download
		signatures[i] = get_url_from_json(signature_as_json);
	}

	json_object_put(response_body_as_json);
	return signatures;
}

std::string getKMSData() {
	//Dados do KMS_DATA
	JsonService* kmsData = new JsonService();
	if (std::string(KMS_TYPE).compare("BRYKMS") == 0) {
		addBRyKMSData(kmsData);
	} else if (std::string(KMS_TYPE).compare("GOVBR") == 0) {
		addGovBRData(kmsData);
	} else if (std::string(KMS_TYPE).compare("DINAMO") == 0) {
		addDinamoData(kmsData);
	} else {
		std::cout << "Error during kms_data initialization - KMS_TYPE not found: " << KMS_TYPE << std::endl;
		exit(1);
	}

	return kmsData->getString();
}

void addBRyKMSData(JsonService * kmsData) {
	if (strlen(PIN) > 0)
		kmsData->addValor("pin", PIN);
	else if (strlen(TOKEN) > 0)
		kmsData->addValor("token", TOKEN);

	if (strlen(UUID_CERT) > 0)
		kmsData->addValor("uuid_cert", UUID_CERT);
	else if (strlen(USER) > 0)
		kmsData->addValor("user", USER);
}

void addGovBRData(JsonService * kmsData) {
	kmsData->addValor("token", TOKEN);
}

void addDinamoData(JsonService * kmsData) {
	kmsData->addValor("uuid_cert", UUID_CERT);
	kmsData->addValor("uuid_pkey", UUID_PKEY);

	if (strlen(TOKEN) > 0)
		kmsData->addValor("token", TOKEN);
	else {
		if (strlen(OTP) > 0)
			kmsData->addValor("otp", OTP);
		kmsData->addValor("user", USER);
		kmsData->addValor("pin", PIN);
	}
}