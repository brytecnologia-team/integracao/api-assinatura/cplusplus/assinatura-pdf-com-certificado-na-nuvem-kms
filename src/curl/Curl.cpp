#include "curl/Curl.h"

Curl::Curl()
{
    curl_global_init(CURL_GLOBAL_ALL);
    curl = curl_easy_init();

    if (!curl) {
        throw new CurlException("Erro em curl_easy_init()"); 
    }
}

Curl::~Curl()
{
    curl_easy_cleanup(curl);
    curl_global_cleanup();
}

void Curl::perform()
{
    res = curl_easy_perform(curl);

    if(res != CURLE_OK) {
        String mensagem = curl_easy_strerror(res);
        mensagem = "curl_easy_perform() failed: " + mensagem;

        throw new CurlException(mensagem);
    }
}

void Curl::setURL(String url) 
{
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
}

long Curl::getHttpCode()
{
    long http_code = 0;
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);

    return http_code;
}

std::string Curl::urlFormat(String value)
{
    String formatedValue = curl_easy_escape(curl, value.c_str(), value.size());
    return formatedValue;
}