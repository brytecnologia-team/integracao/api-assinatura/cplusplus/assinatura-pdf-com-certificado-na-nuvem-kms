#ifndef __service_config_h
#define __service_config_H

#define URL_SERVER "https://hub2.hom.bry.com.br"
// Utilizar https://hub2.bry.com.br para ambiente de produção

#define URL_SERVER_PDF URL_SERVER "/fw/v1/pdf/kms/lote/assinaturas/"

#define ACCESS_TOKEN "<ACCESS_TOKEN>"

#endif
