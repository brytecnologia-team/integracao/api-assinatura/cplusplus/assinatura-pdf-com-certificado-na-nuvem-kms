#ifndef __CURL_SERVICE_H
#define __CURL_SERVICE_H

#include <string>
#include <curl/curl.h>

#include "exception/CurlException.h"

class Curl {
typedef std::string String;

protected:
    CURL * curl;
    CURLcode res;
    String urlFormat(String value);
    
public:
    Curl();
    virtual ~Curl();
    void perform();
    void setURL(String url);
    long getHttpCode();
};

#endif