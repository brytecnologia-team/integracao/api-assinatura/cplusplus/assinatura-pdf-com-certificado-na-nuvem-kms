#ifndef __JSON_ARRAY_SERVICE_H
#define __JSON_ARRAY_SERVICE_H

#include <string>

class JsonArrayService {
typedef std::string String;

private:
    String dados = "";

public:
    void addValor(String value);
    String getString();
};

#endif