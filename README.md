# Geração de assinatura PDF com certificado em nuvem

Este é exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia C++, que utilizam certificado digital instalado em nuvem.

Este exemplo apresenta os passos necessários para a geração de assinatura PDF utilizando-se do certificado em nuvem.

### Formas de acesso ao Certificado em Nuvem

* *BRy KMS*: Certificado está armazenado na Plataforma de Certificados em Nuvem da BRy Tecnologia. Para este modo de acesso ao certificado o Tipo da Credencial deve ser **BRYKMS**. A credencial de acesso pode ser **PIN** ou  **TOKEN**. Para credenciais tipo **PIN** o Valor da Credencial corresponde a senha de acesso ao certificado em nuvem codificada em Base64, enquanto que para credenciais do tipo **TOKEN** o Valor da Credencial corresponde a um token de autorização do acesso ao certificado. Mais informações sobre o serviço de certificados em nuvem podem ser encontradas em nossa documentação da [API de Certificado em Nuvem](https://api-assinatura.bry.com.br/api-certificado-em-nuvem).

* *GovBR*: Certificado está armazenado na Plataforma de Assinatura Digital gov.br. Para este modo de acesso ao certificado o Tipo da Credencial deve ser **GOVBR** e o Valor da Credencial corresponde ao token de acesso do usuário. É possível verificar um exemplo da geração deste token de acesso em nossa [API Autenticação GOV.BR](https://gitlab.com/brytecnologia-team/integracao/api-autenticacao-token-govbr/react/gestao-credenciais).

* *HSM Dinamo*: Certificado está armazenado em um HSM Dinamo. Para este modo de acesso ao certificado o Tipo da Credencial deve ser **DINAMO** e a credencial pode ser token de acesso ou usuário e pin da conta do usuário. Os valores de **UUID_CERT** e **UUID_PKEY** correspondem aos valores de identificação do certificado que realizar a assinatura. É possível verificar um exemplo de como usar as credenciais do DINAMO em nosso [Repositório Swagger da API de Assinatura](https://hub2.bry.com.br/swagger-ui/index.html#/) no schema kms_data.


### Tech

O exemplo utiliza das bibliotecas abaixo:
* [curl/Curl.h] 	- Biblioteca para transferencia de dados
* [json-c/json.h] 	- Biblioteca para dados JSON
* [c++11] 			- Compilador C++11.

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatório a posse de um certificado digital que identifica o autor do artefato assinado que será produzido.

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| ACCESS_TOKEN | Token de acesso (access_token), obtido no BRyCloud (https://api-assinatura.bry.com.br/api-assinatura-digital#autenticacao-apis). Para uso corporativo, o token deve ser da pessoa  jurídica e para uso pessoal, da pessoa física. | include/config/service_config.h
| KMS_TYPE | Tipo de crendecial utilizado no KMS. [Valores Disponíveis: BRYKMS, DINAMO e GOVBR] | include/config/certificate_config.h
| PIN, TOKEN, USER, UUID_CERT, UUID_PKEY e OTP | Configurações de acesso e identificação do certificado em nuvem | include/config/certificate_config.h


# Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  


### Uso

Para execução escolha sua IDE de preferência ou execute por linha de comando, executando os comando abaixo na pasta do projeto:
    make
    ./basic_signature

 [BRy Cloud]: <https://cloud.bry.com.br>